// console.log("Congrats")

// [Section] DOM - Document Object Model
	// allows us to access or modify the properties of an html element in a webpage
	// it is standard on how to get, change, add, or delete HTML elements.
	// we will focus on use of DOM in manageing forms. 

	//For selecting HTML elements, we will be using document.querySelector
		//Syntax: document.querySelector("html element")
		//the querySelector function takes a string input that is formatted like a css selector when applying the styles.

	
	const txtFirstName = document.querySelector("#txt-first-name");
	console.log(txtFirstName);

	const txtLastName = document.querySelector("#txt-last-name");
	console.log(txtLastName);

	const name = document.querySelectorAll(".full-name");
	console.log(name);

	const span = document.querySelectorAll("span");
	console.log(span)

	const text = document.querySelectorAll('input[type]');
	console.log(text);

	const spanFullName = document.querySelector("#fullName")

	const color = document.querySelector("#color")
	

	// const div = document.querySelectorAll(".first-div > span");
	// console.log(div)

		//add class to span if you want to target 1 span only
		/*const div = document.querySelectorAll(".first-div > .second-span");
		console.log(div)
		*/



// [Section] Event Listeners
	// Whenever a user interacts with a webpage, this action is considered as an event.
	// Working with events is large part of creating interactivity in a webpage
	//specify function that perform an action

	// The function use is "addEventListener", it takes 2 arguments
		// first arg - a string identifying event.
		// Second arg - a function that the listener will trigger once the "specified event" is triggered.

	// txtFirstName.addEventListener("keyup", (event) =>{
	// 	console.log(event.target.value); // to get the value that we input in our element 
	// })

	const fullName = () => {
		spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
	}

	txtFirstName.addEventListener("keyup", fullName);
	txtLastName.addEventListener("keyup", fullName);


	const changeColor = () =>{
		spanFullName.style.color = `${color.value}`
	}

	color.addEventListener("click", changeColor)